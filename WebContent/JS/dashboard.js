/**
 * Retrieves the filling levels from the api.
 **/
function getRations() {
  var bowlSize = 100;
  var containerSize = 2000;

    var page = "/SmartFoodDispenser/rest/api/getFillLevelContainer/";
    $.ajax({
        url: page,
        type: "GET",
        cache: false,
        success: function(data) {
            //$("#containerBar").css("height",(data/containerSize*100)+"%");
            $("#containerBar").attr("data-percentage",(data/containerSize*100));
            $("#containerValue").html(data+"g");

            $(function() {
              $("#bars li .bar").each( function( key, bar ) {
                var percentage = $(this).data('percentage');

                $(this).animate({
                  'height' : percentage + '%'
                }, 1000);
              });
            });
        }
    });

    var page = "/SmartFoodDispenser/rest/api/getFillLevelBowl/";
    $.ajax({
        url: page,
        type: "GET",
        cache: false,
        success: function(data) {
            //$("#bowlBar").css("height",(data/bowlSize*100)+"%");
            $("#bowlBar").attr("data-percentage",(data/bowlSize*100));
            $("#bowlValue").html(data+"g");

            $(function() {
              $("#bars li .bar").each( function( key, bar ) {
                var percentage = $(this).data('percentage');

                $(this).animate({
                  'height' : percentage + '%'
                }, 1000);
              });
            });
        }
    });
}
getRations();
