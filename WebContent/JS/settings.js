loadImages();

function loadImages(){
  var folder = "./images/";

  $.ajax({
      url : folder,
      success: function (data) {
          console.log(data);
          $(data).find("a").attr("href", function (i, val) {
              if( val.match(/\.(jpg)$/) ) {
                  //$('#imageGallery').append( "<img src='"+ folder + val +"' width='100px' height='100px' id='"+val.split(".")[0]+"' onClick=\"selectImage('"+val.split(".")[0]+"')\">" );
                  var fileName = val.split(".")[0].split("/");
            	  $('#imageGallery').append( "<img src='"+ val +"' width='100px' height='100px' id='"+fileName[fileName.length-1]+"' onClick=\"selectImage('"+fileName[fileName.length-1]+"')\">" );         
              }
          });
      }
  });
}

function retrainModel(){
	$.ajax({
			url: '/SmartFoodDispenser/rest/api/retrainModel',
			type:'post',
			success:function(){
					alert("Model wird neu trainiert. Dieser Prozess kann einige Minuten dauern.");
			}
	});
}

function deleteImages(){
	if(confirm("Wollen Sie wirklich alle markierten Bilder aus den Trainingsdaten löschen?")){
		selectedImages = $(".selected");
		$(".selected").each(function(index){
			$(this).hide();
			$.ajax({
	        url: '/SmartFoodDispenser/rest/api/fileDelete/'+$(this).attr('id')+'.jpg',
	        type:'GET',
	        success:function(){

	        }
	    });
		});
	}
}

function selectImage(fileName){
	$("#"+fileName).toggleClass("selected");
}

function submitPicture(){
    $.ajax({
        url: '/SmartFoodDispenser/rest/api/fileUpload',
        type:'post',
        data: new FormData($('#imageForm')[0]),
				contentType: false,
				processData:false,
        success:function(){
			       location.reload();
        }
    });
};

document.getElementById("uploadBtn").onchange = function () {
    document.getElementById("uploadFile").value = this.files[0].name;
};
