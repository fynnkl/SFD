jQuery(function($) {
    var $bodyEl = $('body'),
        $sidedrawerEl = $('#sidedrawer');


    // ==========================================================================
    // Toggle Sidedrawer
    // ==========================================================================
    function showSidedrawer() {
        // show overlay
        var options = {
            onclose: function() {
                $sidedrawerEl
                    .removeClass('active')
                    .appendTo(document.body);
            }
        };

        var $overlayEl = $(mui.overlay('on', options));

        // show element
        $sidedrawerEl.appendTo($overlayEl);
        setTimeout(function() {
            $sidedrawerEl.addClass('active');
        }, 20);
    }


    function hideSidedrawer() {
        $bodyEl.toggleClass('hide-sidedrawer');
    }


    $('.js-show-sidedrawer').on('click', showSidedrawer);
    $('.js-hide-sidedrawer').on('click', hideSidedrawer);


    // ==========================================================================
    // Animate menu
    // ==========================================================================
    var $titleEls = $('strong', $sidedrawerEl);

    $titleEls
        .next()
        .hide();

    $titleEls.on('click', function() {
        $(this).next().slideToggle(200);
    });
});

/**
 * Hides the sidedrawer.
 **/
function hideSD() {
    var $sidedrawerEl = $('#sidedrawer');

    $sidedrawerEl
        .removeClass('active')
        .appendTo(document.body);


    mui.overlay('off');
}

/**
 * Shows the sidedrawer.
 **/
function showSD() {
    var $bodyEl = $('body'),
        $sidedrawerEl = $('#sidedrawer');
    // show overlay
    var options = {
        onclose: function() {
            $sidedrawerEl
                .removeClass('active')
                .appendTo(document.body);
        }
    };

    var $overlayEl = $(mui.overlay('on', options));

    // show element
    $sidedrawerEl.appendTo($overlayEl);
    setTimeout(function() {
        $sidedrawerEl.addClass('active');
    }, 20);
}

/**
 * Sets a cookie.
 * @param {string} cname The name of the cookie.
 * @param {string} cvalue The value of the cookie.
 * @param {int} exdays The amount of days that the cookie should be saved.
 **/
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * Retrieves a cookie.
 * @param {string} name The name of the cookie.
 * @returns The cookie.
 **/
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

/**
 * Deletes a cookie.
 * @param {string} name The name of the cookie.
 **/
function eraseCookie(name) {
    document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}

var refreshInterval;

/**
 * Displays a subpage in the main body of the page.
 * @param {string} title The title of the subpage, which is set as website title.
 * @param {string} page The path to the page file.
 **/
function showSubPage(title, page) {
    clearInterval(refreshInterval);
    document.title = title;

    $.ajax({
        url: page,
        type: "GET",
        cache: false,
        success: function(data) {
            $('#main').html(data);
        }
    });
}

/**
 * Retrieves a GET-parameter from the url.
 * @param {string} parameterName The name of the GET-parameter.
 **/
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

/**
 * Checks if a hash is set for the page, so that the corresponding subpage is opened automatically.
 **/
function checkHash() {
    // Check URL using setTimeout as it may not change before
    // listener is called
    window.setTimeout(doHashCheck, 10);
}

/**
 * Calls the function with the name of the hash.
 **/
var doHashCheck = (function(global) {
    return function() {
        var fnName = window.location.hash.replace(/^#/, '');
        // fnName should be a native function, not a host method
        if (typeof global[fnName] == 'function') {
            global[fnName]();
        } else {
            dashboard();
        }
    }
})(this);

/**
 * Opens the subpage 'Dashboard'.
 **/
function dashboard() {
    showSubPage("SFD - Dashboard", "./pages/dashboard.html");
}

/**
 * Opens the subpage 'Rationen'.
 **/
function servings() {
    showSubPage("SFD - Rationen", "./pages/servings.html");
}

/**
 * Opens the subpage 'Logs'.
 **/
function logs() {
    showSubPage("SFD - Logs", "./pages/logs.html");
}

/**
 * Opens the subpage 'Einstellungen'.
 **/
function settings() {
    showSubPage("SFD - Einstellungen", "./pages/settings.html");
}

$("#pseudobar").on("swiperight", function() {
    showSD();
});

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function activateModal() {
    // initialize modal element
    var modalEl = document.createElement('div');
    modalEl.innerHTML = '<br><p>Logo uses icon made by <a href="http://www.freepik.com/">FreePik</a> for <a href="http://www.flaticon.com">FlatIcon.com</a></p>';
    modalEl.style.width = '400px';
    modalEl.style.height = '300px';
    modalEl.style.margin = '100px auto';
    modalEl.style.backgroundColor = '#fff';
    
    // show modal
    mui.overlay('on', modalEl);
  }
