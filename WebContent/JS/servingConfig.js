var servingId = findGetParameter("servingId");
$("#cancel").click(function() {
    location.replace("/SmartFoodDispenser/#servings");
    location.reload();
});
/**
 * Opens the view for editing or adding a serving, depending on if a GET-parameter is set.
 **/
function checkServingId() {
    var page;
    if (servingId !== null) {
        $("#pageInfo").text("Ration bearbeiten");
        page = "/SmartFoodDispenser/rest/api/getServing/" + servingId;
        $.ajax({
            url: page,
            type: "GET",
            cache: false,
            success: function(data) {
                var date = new Date(data.time);
                $("#time").val(pad(date.getHours(),2)+":"+pad(date.getMinutes(),2));
                $("#amount").val(data.amount);
            },
            error: function(data) {
                alert("Ration mit entsprechender ID nicht gefunden.");
            }
        });
    } else {
        $("#pageInfo").text("Ration hinzufügen");
    }
}

/**
 * Sends the new or changed serving to the server.
 **/
function saveServing() {
	  var page;
    var serving = new Object();
    serving.time = new Date(new Date().toString().split(":")[0].slice(0,-2)+$("#time").val()+":00");
    serving.amount = $("#amount").val();

		if (servingId === null) {
			page = "/SmartFoodDispenser/rest/api/addServing";
		} else {
			page = "/SmartFoodDispenser/rest/api/editServing";
			serving.id = servingId;
		}


		$.ajax({
			url: page,
			type: "POST",
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(serving),
			cache: false,
			success: function(data) {
				location.replace("/SmartFoodDispenser/?noCache=true#servings");
			},
			error: function(data) {
			}
		});
}

checkServingId();
