/**
 * Retrieves the rations from the api.
 **/
function getServings() {
    var page = "/SmartFoodDispenser/rest/api/getAllServings/";
    $.ajax({
        url: page,
        type: "GET",
        cache: false,
        success: function(data) {
            fillServingsTable(data);
        }
    });
}

function fillServingsTable(data){
  $("#servingsTable tbody").html("");
  for (var i = 0; i < data.length; i++) {
      $("#servingsTable tbody").append("<tr><td>"+new Date(data[i]["time"]).toLocaleTimeString()+"</td><td>"+data[i]["amount"]+"</td><td><a href='?servingId="+data[i]["id"]+"#servings'>Bearbeiten</a> / <a href='#servings' onClick='deleteServing("+data[i]["id"]+")'>Löschen</a></td></tr>");
  }
}

function deleteServing(id){
  if(confirm("Wollen Sie die Ration wirklich löschen?")){
    var page = "/SmartFoodDispenser/rest/api/removeServing/";
    $.ajax({
        url: page,
        type: "POST",
        contentType: "application/json",
        cache: false,
        data: '{"id":'+id+',"time":"","amount":0}',
        success: function(data) {
            getServings();
        }
    });
  }
}


/**
 * Opens the view for editing or adding a serving, depending on if a GET-parameter is set.
 **/
function checkServingId() {
    var servingId = findGetParameter("servingId");
    if (servingId !== null) {
        showSubPage('SFD - Ration konfigurieren', './pages/servingConfig.html');
    }
}

checkServingId();
getServings();
