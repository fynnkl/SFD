/**
 * Retrieves the eating log from the api.
 **/
function getEatingLog() {
    var page = "/SmartFoodDispenser/rest/api/getAllEatingLogs/";
    $.ajax({
        url: page,
        type: "GET",
        cache: false,
        success: function(data) {
          fillEatingLogTable(data);
          fillEatingLogChart(data);
        }
    });
}

function fillEatingLogTable(data){
	$("tbody").html("");
  for (var i = 0; i < data.length; i++) {
      $("tbody").append("<tr><td>"+new Date(data[i]["time"]).toLocaleString()+"</td><td>"+data[i]["amount"]+"</td></tr>");
  }
}

function fillEatingLogChart(data){
  for(i = 0; i < data.length; i++){
    data[i].t = data[i].time;
    delete data[i].time;
    data[i].y = data[i].amount;
    delete data[i].amount;
    delete data[i].id;
  }

  var myChart = new Chart(document.getElementById("logChart"), {
      "type": "line",
      "data": {
          "datasets": [{
              "label": "Amount of food eaten (g)",
              "data": data,
              "fill": false,
              "borderColor": "#ff4081",
              "lineTension": 0.1
          }]
      },
      "options": {
          "scales": {
              "xAxes": [{
                  "type": 'time',
                  "distribution": 'series'
              }]
          }
      }
  });
}

getEatingLog();
