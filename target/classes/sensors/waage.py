import serial
import time

ser = serial.Serial('/dev/serial/by-path/platform-3f980000.usb-usb-0:1.5:1.0', 9600)
while(not ser.inWaiting()):
    time.sleep(0.1)
    ser.write('GET weights;')
    ser.flush()

eins = ser.readline()
eins = eins.strip()
zwei = ser.readline()
zwei = zwei.strip()

ser.close()

print(str(eins))
print(str(zwei))
