package api;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import datamanagement.DataManager;

/**  
* ApplicationConfig.java - Loads all classes and features that should be deployed to the server.
* @author  Fynn Kloepper
* @version 1.0 
*/ 
@ApplicationPath("rest")
public class ApplicationConfig extends ResourceConfig {
	public ApplicationConfig() {
		this.packages("api");
		this.register(MultiPartFeature.class);
		
		System.out.println("Starting Server");
		DataManager dm = new DataManager();
		dm.checkForEatingLogAddition();
		dm.startAllServingThreads();
	}
}