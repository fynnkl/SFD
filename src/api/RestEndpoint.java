package api;

import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import datamanagement.DataManager;
import datamodel.EatingLog;
import datamodel.Serving;

/**  
* RestEndpoint.java - Provides the methods for a signed in user. 
* @author  Fynn Kloepper
* @version 1.0 
*/ 
@Path("/api/")
public class RestEndpoint {
	private DataManager dm = new DataManager();
	
	/** 
	 * Get all servings and their time.
	 * @return Returns a JSON which contains data of all servings.
	 */
	@GET
	@Path("/getAllServings")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Serving> getAllServings() {
		return dm.getAllServings();
	}
	
	/** 
	 * Get all servings and their time.
	 * @return Returns a JSON which contains data of all servings.
	 */
	@GET
	@Path("/getAllEatingLogs")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EatingLog> getAllEatingLogs() {
		return dm.getAllEatingLogs();
	}
	
	/** 
	 * Add a serving.
	 * @param serving - the serving that should be added.
	 * @return Returns corresponding status code.
	 */
	@POST
	@Path("/addServing")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addServing(Serving serving) {
		if(dm.addServing(serving) != null) {
			return Response.status(200).build();
		} else {
			return Response.status(400).entity("Serving could not be added").build();
		}
	}
	
	/** 
	 * Add an eating log.
	 * @param eatingLog - the eatingLog that should be added.
	 * @return Returns corresponding status code.
	 */
	@POST
	@Path("/addEatingLog")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addEatingLog(EatingLog eatingLog) {
		if(dm.addEatingLog(eatingLog) != null) {
			return Response.status(200).build();
		} else {
			return Response.status(400).entity("EatingLog could not be added").build();
		}
	}
	
	/** 
	 * Get a specific Serving by his ID.
	 * @param id - the id of the serving.
	 * @return Returns a JSON which contains 
	 * 			data of the corresponding serving.
	 */
	@GET
	@Path("/getServing/{id}") 
	@Produces(MediaType.APPLICATION_JSON)
	public Serving getServing(@PathParam("id") int id) {
		return dm.getServingById(id);
	}
	
	/** 
	 * Get a specific EatingLog by his ID.
	 * @param id - the id of the eatingLog.
	 * @return Returns a JSON which contains data of the corresponding eatingLog.
	 */
	@GET
	@Path("/getEatingLog/{id}") 
	@Produces(MediaType.APPLICATION_JSON)
	public EatingLog getEatingLog(@PathParam("id") int id) {
		return dm.getEatingLogById(id);
	}
	
	/** 
	 * Edit a serving.
	 * @param serving - the changed serving object.
	 * @return Returns corresponding status code.
	 */
	@POST
	@Path("/editServing")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editServing(Serving serving) {
		if (dm.editServing(serving) != null) {
			return Response.status(200).build();
		} else {
			return Response.status(400)
					.entity("No serving with the provided id found.").build();
		}
	}
	
	/** 
	 * Edit a eatingLog.
	 * @param eatingLog - the changed eatingLog object.
	 * @return Returns corresponding status code.
	 */
	@POST
	@Path("/editEatingLog")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editEatingLog(EatingLog eatingLog) {
		if (dm.editEatingLog(eatingLog) != null) {
			return Response.status(200).build();
		} else {
			return Response.status(400)
					.entity("No eatingLog with the provided id found.").build();
		}
	}
	
	/** 
	 * Remove a serving.
	 * @param serving - A serving object, which holds the id of the serving that should get deleted.
	 * @return Returns corresponding status code.
	 */
	@POST
	@Path("/removeServing")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeEmployee(Serving serving) {
		if(dm.removeServing(serving) == true) {
			return Response.status(200).build();	
		} else {
			return Response.status(400).entity("Faulty id provided.").build();
		}
	}	
	
	/** 
	 * Remove a eatingLog.
	 * @param eatingLog - A eatingLog object, which holds the id of the eatingLog that should get deleted.
	 * @return Returns corresponding status code.
	 */
	@POST
	@Path("/removeEatingLog")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeEatingLog(EatingLog eatingLog) {
		if(dm.removeEatingLog(eatingLog) == true) {
			return Response.status(200).build();	
		} else {
			return Response.status(400).entity("Faulty id provided.").build();
		}
	}
	
	
	/**
	 * Saves the provided picture of the corresponding event or employee. 
	 * @param uploadedInputStream - the file itself.
	 * @param body - contains the MimeType of the file.
	 * @return An appropriate status code as a response.
	 */
	@POST
	@Path("/fileUpload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
	        @FormDataParam("file") InputStream uploadedInputStream,
	        @FormDataParam("file") final FormDataBodyPart body) {	
		
		String mimeType = body.getMediaType().toString();		
		if(mimeType.equals("image/jpeg")) {
		    dm.savePicture(uploadedInputStream);		
		    return Response.status(200).build();
		} else {
			return Response.status(400).entity("Only JPG files are accepted.").build();
		}
	}
	
	/**
	 * Saves the provided picture of the corresponding event or employee. 
	 * @param uploadedInputStream - the file itself.
	 * @param body - contains the MimeType of the file.
	 * @return An appropriate status code as a response.
	 */
	@GET
	@Path("/fileDelete/{fileName}")
	public Response deleteFile(@PathParam("fileName") String fileName) {	
		if(dm.deletePicture(fileName)) {
			return Response.status(200).build();
		} else {
			return Response.status(400).entity("File not found.").build();
		}
	}
	
	/** 
	 * Checks the filling level of the container.
	 * @return Returns the filling level in amount of grams. 
	 */
	@GET
	@Path("/getFillLevelContainer")
	public Response getFillLevelContainer() {
		return Response.ok(dm.getFillLevelContainer()).build();
	}
	
	/** 
	 * Checks the filling level of the container.
	 * @return Returns the filling level in amount of grams. 
	 */
	@GET
	@Path("/getFillLevelBowl")
	public Response getFillLevelBowl() {
		return Response.ok(dm.getFillLevelBowl()).build();
	}
	
	/** 
	 * Retrains the model based on the new images to recognize the pet.
	 */
	@POST
	@Path("/retrainModel")
	public Response retrainModel() {
		dm.retrainModel();
		return Response.ok().build();
	}
	
}