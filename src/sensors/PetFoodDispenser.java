package sensors;

public class PetFoodDispenser {

	private int fillLevelContainer;
	private int fillLevelBowl;
	private Camera camera; 
	private Waage waage; 
	
	public PetFoodDispenser(){
		this.fillLevelContainer = 0;
		this.fillLevelBowl = 0;
		this.waage = new Waage(); 
		this.camera = new Camera(); 
	}
	
	public boolean retrainModel(){
		return true;
	}
	
	public int getFillLevelContainer(){
		/*this.requestFillLevels();
		//START mock
		this.fillLevelContainer = (this.fillLevelContainer + 1300) % 2000;
		// END mock
		return this.fillLevelContainer;*/
		return waage.getFillLevelContainer();
	}
	
	public int getFillLevelBowl(){
		/*this.requestFillLevels();
		//START mock
		this.fillLevelBowl = (this.fillLevelBowl + 10) % 100;
		//END mock
		return this.fillLevelBowl;*/
		return waage.getFillLevelBowl();
	}
	
	private void requestFillLevels(){
		
	}
	
	public boolean outputServing(int grams) {
		System.out.println("Output food: "+grams+"g");
		if(this.fillLevelBowl >= grams) {
			this.fillLevelBowl -= grams;
			return true;
		}
		return false;
	}
	
}
