package sensors;

public class Camera{
    private final String path = "/home/pi/Desktop/SmartFoodDispenser/camera/tmp_images/tmp.jpg";
    
    public boolean take_image(){
        String command = "raspistill -n -o "+ path;
        try{
            Runtime.getRuntime().exec(command);
            return true;
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
    }
    
    public static void main(String[] args){
        Camera c = new Camera();
        c.take_image();
    }
}