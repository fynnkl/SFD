package sensors; 

import java.io.*;

public class Waage{

private int[] weights;

public Waage(){
    this.weights = new int[2];
    this.getWeights();
}

public int getFillLevelContainer(){
    this.getWeights();
    return this.weights[0];
}

public int getFillLevelBowl(){
    this.getWeights();
    return this.weights[1];
}

public void getWeights(){
    try{
        Process p = Runtime.getRuntime().exec("python /home/pi/Desktop/server/apache-tomcat-7.0.90/webapps/SmartFoodDispenser/WEB-INF/classes/sensors/waage.py");
        BufferedReader std = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String s = null;
		for(int i = 0;(s = std.readLine()) != null && i < this.weights.length; i++){
		    this.weights[i] = (int)(Double.parseDouble(s)*100);
		}
		System.out.println(this.weights[0]);
		System.out.println(this.weights[1]);
    }catch(Exception e){
    	e.printStackTrace();
    }
}

public static void main(String[] args){
    Waage w = new Waage();
    System.out.println(w.getFillLevelBowl());
    System.out.println(w.getFillLevelContainer());
}

}