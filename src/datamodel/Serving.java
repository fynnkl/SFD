package datamodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**  
* Serving.java - A JavaBean, which represents the database table "Serving"
* @author  Fynn Kloepper
* @version 1.0 
*/ 
@Entity
@Table(name = "serving", schema = "SmartFoodDispenser")
@NamedQueries({
	@NamedQuery(name="Serving.findAll", query="select e from Serving e"),
   	})
public class Serving implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Basic
	@Column(name = "time", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date time;
	@Basic
	@Column(name = "amount", nullable = false)
	private int amount;
	
	public Serving() {
		
	}
	
	public Serving(Date time, int amount) {
		this.time = time;
		this.amount = amount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Serving [id=" + id + ", time=" + time + ", amount=" + amount + "]";
	}
	
	
}
