package datamanagement;

import java.security.SecureRandom;
import java.util.Objects;

/**  
* RandomName.java - Generates a token consisting of 16 random alphanumeric characters. 
* @author  Fynn Kloepper
* @version 1.0 
*/ 
public class RandomName {
    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = upper.toLowerCase();

    public static final String digits = "0123456789";

    public static final String alphanum = upper + lower + digits;

    private final SecureRandom random;

    private final char[] symbols;

    private final char[] buffer;
    
    private final int length = 16; 

    /**
     * Alphanumeric string generator.
     */
    public RandomName() {
        this.random = Objects.requireNonNull(new SecureRandom());
        this.symbols = alphanum.toCharArray();
        this.buffer = new char[length];
    }
     
    /**
     * Generate a random string.
     * @return a new random string.
     */
    public String nextString() {
        for (int i = 0; i < buffer.length; ++i) {
        	buffer[i] = symbols[random.nextInt(symbols.length)];
        }            
        return new String(buffer);
    }
}
