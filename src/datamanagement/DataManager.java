package datamanagement;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContext;

import datamodel.EatingLog;
import datamodel.Serving;
import sensors.PetFoodDispenser;

public class DataManager {
	private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "SmartFoodDispenser" );
	private PetFoodDispenser pfd = new PetFoodDispenser();
	private ArrayList<Timer> timers = new ArrayList<Timer>(); 
	
	/**
     * Adds a new serving to the database.
     * @param serving - the serving object that will be added.
     * @return the serving object.
     */
    public Serving addServing(Serving serving){  
		EntityManager em = emfactory.createEntityManager( );
    	try {
			if (serving != null) {
				if (serving.getTime() != null && serving.getAmount() > 0) {
					serving.setId(null);
					em.getTransaction( ).begin( );					
				    em.persist(serving);
				    em.getTransaction( ).commit( );
				    startAllServingThreads(); //Threads neustarten
					return serving;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			em.close();
		}
    	return null;
    }
    
    /**
     * Adds a new eatingLog to the database.
     * @param eatingLog - the eatingLog object that will be added.
     * @return the eatingLog object.
     */
    public EatingLog addEatingLog(EatingLog eatingLog){  
		EntityManager em = emfactory.createEntityManager( );
    	try {
			if (eatingLog != null) {
				if (eatingLog.getTime() != null) {
					eatingLog.setId(null);
					em.getTransaction( ).begin( );					
				    em.persist(eatingLog);
				    em.getTransaction( ).commit( );
					return eatingLog;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			em.close();
		}
    	return null;
    }
	
    /**
     * Retrieves all servings from the database.
     * @return a list of serving objects.
     */
    @SuppressWarnings("unchecked")
	public List<Serving> getAllServings(){
		EntityManager em = emfactory.createEntityManager( );
    	try {
			return (List<Serving>) em.createNamedQuery("Serving.findAll").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			em.close();
		}
		return null;	
	}
	
    /**
     * Retrieves all eatingLogs from the database.
     * @return a list of eatingLog objects.
     */
	@SuppressWarnings("unchecked")
	public List<EatingLog> getAllEatingLogs(){
		EntityManager em = emfactory.createEntityManager( );
    	try {
			return (List<EatingLog>) em.createNamedQuery("EatingLog.findAll").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			em.close();
		}
		return null;	
	}
	
    /**
     * Deletes an EatingLog from the database.
     * @param eatingLog - the eatingLog object that will be deleted.
     * @return true if transaction was successful.
     */
    public boolean removeEatingLog(EatingLog eatingLog){
    	EntityManager em = emfactory.createEntityManager( );
    	try {
			if (eatingLog != null) {
				if (eatingLog.getId() != null) {
					EatingLog eatingLogDBCopy = em.find(EatingLog.class, eatingLog.getId());
					if (eatingLogDBCopy != null) {
						em.getTransaction( ).begin( );
					    em.remove(eatingLogDBCopy);
					    em.getTransaction( ).commit( );
						return true;
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			em.close();
		}
    	return false;
    }
    
    /**
     * Deletes a serving from the database.
     * @param serving - the serving object that will be deleted.
     * @return true if transaction was successful.
     */
    public boolean removeServing(Serving serving){
    	EntityManager em = emfactory.createEntityManager( );
    	try {
			if (serving != null) {
				if (serving.getId() != null) {
					Serving servingDBCopy = em.find(Serving.class, serving.getId());
					if (servingDBCopy != null) {
						em.getTransaction( ).begin( );
					    em.remove(servingDBCopy);
					    em.getTransaction( ).commit( );
					    startAllServingThreads(); //Threads neustarten
						return true;
					}					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			em.close();
		}
    	return false;
    }
    
    /**
     * Get an eatingLog from the database by id
     * @param id - the id of the eatingLog
     * @return the eatingLog object
     */
    public EatingLog getEatingLogById(int id) {
    	EntityManager em = emfactory.createEntityManager( );
    	try {
			return em.find(EatingLog.class, id);			
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			em.close();
		}
    	return null; 
    }
    
    /**
     * Get a serving from the database by id
     * @param id - the id of the serving
     * @return the serving object
     */
    public Serving getServingById(int id) {
    	EntityManager em = emfactory.createEntityManager( );
    	try {
			return em.find(Serving.class, id);			
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			em.close();
		}
    	return null; 
    }
    
    
    /**
     * Edits an eatingLog in the database.
     * @param changedLog - the EatingLog object with the new values.
     * @return the changed EatingLog.
     */
    public EatingLog editEatingLog(EatingLog changedLog){  
		EntityManager em = emfactory.createEntityManager( );
    	try {
    		if(changedLog.getId() != null) {
    			EatingLog oldLog = em.find(EatingLog.class, changedLog.getId());
				if(oldLog != null){
					oldLog.setTime(changedLog.getTime());
					oldLog.setAmount(changedLog.getAmount());
					em.getTransaction( ).begin( );					
				    em.persist(oldLog);
				    em.getTransaction( ).commit( );
					return oldLog;				
				}		
    		}
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			em.close();
		}
    	return null;
    }
    
    /**
     * Edits a serving in the database.
     * @param changedServing - the Serving object with the new values.
     * @return the changed Serving.
     */
    public Serving editServing(Serving changedServing){  
		EntityManager em = emfactory.createEntityManager( );
    	try {
    		if(changedServing.getId() != null) {
    			Serving oldServing = em.find(Serving.class, changedServing.getId());
				if(oldServing != null){
					oldServing.setTime(changedServing.getTime());
					oldServing.setAmount(changedServing.getAmount());
					em.getTransaction( ).begin( );					
				    em.persist(oldServing);
				    em.getTransaction( ).commit( );
				    startAllServingThreads(); //Threads neustarten
					return oldServing;				
				}		
    		}
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			em.close();
		}
    	return null;
    }
    
    /**
     * Saves pictures to train the new model
     * @param uploadedInputStream - the image file
     * @param uploadedFileName - the name of the file
     * @return the path to the file.
     */
    public String savePicture(InputStream uploadedInputStream) {
    	String path = null;
        try {
            OutputStream out = null;
            int read = 0;
            byte[] bytes = new byte[1024];
            
            File images = new File("images");
            // if the directory does not exist, create it
            if (!images.exists()) {
                try{
                    images.mkdir();
                } 
                catch(SecurityException e){
                	e.printStackTrace();
                }
            }
            
            RandomName gen = new RandomName();
    		String fileName = gen.nextString()+".jpg";
            
            //File image = new File("C:\\xampp\\tomcat\\webapps\\SmartFoodDispenser\\images\\"+fileName);
            File image = new File("/home/pi/Desktop/server/apache-tomcat-7.0.90/webapps/SmartFoodDispenser/images/"+fileName);
            image.createNewFile();
            path = image.getAbsolutePath();
            System.out.println(path);
            out = new FileOutputStream(image);
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {

            e.printStackTrace();
        } 
        return path;

    }
    
   /**
    * Deletes pictures from the training model
    * @param fileName - the name of the picture that should get deleted
    */
   public boolean deletePicture(String fileName) {
	   try{
		   	//File file = new File("C:\\xampp\\htdocs\\SmartFoodDispenser\\images/"+fileName);  
		   	File file = new File("/home/pi/Desktop/server/apache-tomcat-7.0.90/webapps/SmartFoodDispenser/images/"+fileName);
	   		return file.delete();
	   	}catch(Exception e){
	   		e.printStackTrace();
	   		return false;
	   	}   
   }
    
    /**
     * Starts a Thread, which checks the fill level of the bowl every five minutes and writes the new fill level into the database. 
     */
    public void checkForEatingLogAddition() {
    	Runnable runnable = new Runnable() {
    		int lastWeight = getFillLevelBowl();
    	      public void run() {
    	    	  System.out.println("EatingLog Check");
    	    	  int newWeight = getFillLevelBowl();
    	    	 if(newWeight < lastWeight) {  
	    	        EatingLog el = new EatingLog(new Date(), (lastWeight - newWeight));
	    	        addEatingLog(el);
	    	        lastWeight = newWeight; 
    	    	}
    	      }
    	    };
	    ScheduledExecutorService service = Executors
	                    .newSingleThreadScheduledExecutor();
	    service.scheduleAtFixedRate(runnable, 0, 5, TimeUnit.MINUTES);
    }
    
    /**
     * Cancels all existing timers and starts new timers for all existing servings. 
     */
    public void startAllServingThreads() {
    	for (Timer timer: timers) {
    		timer.cancel();
    	}
    	
    	List<Serving> servings = getAllServings();
    	for (Serving serving: servings) {
    	    startServingThread(serving);
    	}
    }
    
    /**
     * Starts a timer for a serving
     * @param serving - the serving that has to be output at a specific time. 
     */
    public void startServingThread(Serving serving) {
    	Timer timer = new Timer();
    	timers.add(timer);
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, serving.getTime().getHours());
        date.set(Calendar.MINUTE, serving.getTime().getMinutes());
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        if(date.getTime().before(new Date())) {
        	date.add(Calendar.DATE, 1);
        }
        timer.schedule(
          new TimerTask() {
			@Override
			public void run() {
				pfd.outputServing(serving.getAmount());
			}        	  
          },
          date.getTime(),
          1000 * 60 * 60 * 24
        );
    }
    
    /**
     * Retrains the tensorflow model with the new images.
     */
    public void retrainModel() {
    	pfd.retrainModel();
    }
    
    /**
     * Retrieves the filling level of the container.
     */
    public int getFillLevelContainer() {
    	return pfd.getFillLevelContainer();
    }
    
    /**
     * Retrieves the filling level of the bowl. 
     */
    public int getFillLevelBowl() {
    	return pfd.getFillLevelBowl();
    }
}
